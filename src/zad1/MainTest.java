package zad1;

public class MainTest {
	public static void main(String[] args) {
		String[] text = { "Ala ", "ma ", "kota ", "a ", "kot ", "ma ", "Ale", "i ","Basie" };
		Author autor = new Author(text);
		new Thread(autor).start();
		new Thread(new Writer(autor)).start();
	}
}