/**
 *
 *  @author Ritz Szymon S12910
 *
 */

package zad1;

import java.util.concurrent.BlockingQueue;

public class Writer implements Runnable {

	BlockingQueue<String> queue = null;

	public Writer(Author autor) {
		this.queue = autor.getQueue();

	}

	@Override
	public void run() {
		int size = queue.size();
		for (int i = 0; i < size; i++) {
			try {
				System.out.println(queue.take());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
}
