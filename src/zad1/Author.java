/**
 *
 *  @author Ritz Szymon S12910
 *
 */

package zad1;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Author implements Runnable {

	int size = 0;
	String[] text;
	BlockingQueue<String> queue;

	public Author(String[] args) {
		text = args;
		queue = new ArrayBlockingQueue<String>(args.length);

	}

	public BlockingQueue<String> getQueue() {
		return queue;
	}

	@Override
	public void run() {

		for (int i = 0; i < text.length; i++) {
			try {
				queue.put(text[i]);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
